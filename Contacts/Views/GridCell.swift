//
//  GridCell.swift
//  Contacts
//
//  Created by Andrii Lisitsyn on 5/26/20.
//  Copyright © 2020 Andrii Lisitsyn. All rights reserved.
//

import UIKit

class GridCell: UICollectionViewCell {
    @IBOutlet weak var avatarImg: UIImageView!
    var statusView: StatusView!
    
    override func awakeFromNib() {
        statusView = StatusView(frame: CGRect(x: 30, y : 30, width: 17, height: 17))
        self.addSubview(statusView)
    }
    
    func updateViews(person: Person) {
        avatarImg.downloaded(from: Gravatar.instance.imageLink(email: person.email))
        statusView.setColor(status: person.status)
    }
}
