//
//  StatusView.swift
//  Contacts
//
//  Created by Andrii Lisitsyn on 5/27/20.
//  Copyright © 2020 Andrii Lisitsyn. All rights reserved.
//

import UIKit

class StatusView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        layer.borderWidth = 2
        layer.masksToBounds = false
        layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        layer.cornerRadius = frame.height/2
        clipsToBounds = true
    }

    func setColor(status: Person.Status)
    {
        switch status {
        case Person.Status.online:
            backgroundColor = #colorLiteral(red: 0.1548937559, green: 0.7927166224, blue: 0.2526278794, alpha: 1)
        case Person.Status.offline:
            backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
