//
//  CircleImage.swift
//  Contacts
//
//  Created by Andrii Lisitsyn on 5/27/20.
//  Copyright © 2020 Andrii Lisitsyn. All rights reserved.
//

import UIKit

class CircleImage: UIImageView {
    override init(image: UIImage?) {
        super.init(image: image)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.frame.size.height / 2
        self.clipsToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
