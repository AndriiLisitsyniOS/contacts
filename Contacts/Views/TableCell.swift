//
//  PersonCell.swift
//  Contacts
//
//  Created by Andrii Lisitsyn on 5/25/20.
//  Copyright © 2020 Andrii Lisitsyn. All rights reserved.
//

import UIKit

class TableCell: UITableViewCell {
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    var statusView: StatusView!
    
    override func awakeFromNib() {
        statusView = StatusView(frame: CGRect(x: 40, y : 30, width: 17, height: 17))
        self.addSubview(statusView)
    }
    
    func updateViews(person: Person) {
        avatarImg.downloaded(from: Gravatar.instance.imageLink(email: person.email))
        nameLbl.text = person.name
        statusView.setColor(status: person.status)
    }
}
