//
//  DetailedInfoVC.swift
//  Contacts
//
//  Created by Andrii Lisitsyn on 5/26/20.
//  Copyright © 2020 Andrii Lisitsyn. All rights reserved.
//

import UIKit

class DetailedInfoVC: UIViewController {
    
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    
    private(set) var person: Person?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updateViews()
    }
    
    func initPerson(person: Person)
    {
        self.person = person
    }
    
    func updateViews() {
        avatarImg.downloaded(from: Gravatar.instance.largeImageLink(email: person!.email))
        nameLbl.text = person!.name
        statusLbl.text = person!.status.rawValue
    }

    @IBAction func backBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
