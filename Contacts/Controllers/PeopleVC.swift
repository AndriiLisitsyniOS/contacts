//
//  ViewController.swift
//  Contacts
//
//  Created by Andrii Lisitsyn on 5/25/20.
//  Copyright © 2020 Andrii Lisitsyn. All rights reserved.
//

import UIKit

class PeopleVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var peopleList: UITableView!
    @IBOutlet weak var peopleGrid: UICollectionView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        peopleList.dataSource = self
        peopleList.delegate = self
        peopleGrid.dataSource = self
        peopleGrid.delegate = self
        
        People.instance.addPeople(peopleNum: 50)
        
        let font = UIFont.systemFont(ofSize: 16)
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
    }
    
    @IBAction func segmentedControlSwitched(_ sender: Any) {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            peopleList.isHidden = false
            peopleGrid.isHidden = true
        case 1:
            peopleList.isHidden = true
            peopleGrid.isHidden = false
        default:
            break;
        }
    }
    
    @IBAction func simulateChangesBtnPressed(_ sender: Any) {
        People.instance.changeStatuses()
        People.instance.changeNames()
        
        let peopleToRemove = Int.random(in: 1..<20)
        let peopleToAdd = Int.random(in: 1..<20)
        People.instance.removePeople(peopleNum: peopleToRemove)
        People.instance.addPeople(peopleNum: peopleToAdd)
        
        peopleList.reloadData()
        peopleGrid.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        People.instance.getPeople().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell") as? TableCell {
            let person = People.instance.getPeople()[indexPath.row]
            cell.updateViews(person: person)
            return cell
        } else {
            return TableCell()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let person = People.instance.getPeople()[indexPath.row]
        performSegue(withIdentifier: "DetailedInfoVC", sender: person)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailedInfoVC"
        {
            if let detailedInfoVC = segue.destination as? DetailedInfoVC {
                let person = sender as! Person
                detailedInfoVC.initPerson(person: person)
                detailedInfoVC.modalPresentationStyle = .fullScreen
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        People.instance.getPeople().count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GridCell", for: indexPath) as? GridCell {
            let person = People.instance.getPeople()[indexPath.row]
            cell.updateViews(person: person)
            return cell
        }
        
        return GridCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let person = People.instance.getPeople()[indexPath.row]
        performSegue(withIdentifier: "DetailedInfoVC", sender: person)
    }    
}

