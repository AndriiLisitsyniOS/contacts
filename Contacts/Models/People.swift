//
//  People.swift
//  Contacts
//
//  Created by Andrii Lisitsyn on 5/27/20.
//  Copyright © 2020 Andrii Lisitsyn. All rights reserved.
//

import Foundation

class People
{
    static let instance = People()
    
    private var people = [Person]()
    
    private let firstNames = ["Rachel", "Monica", "Phoebe", "Joey", "Chandler", "David"]
    private let lastNames = ["Green", "Geller", "Buffay", "Tribbiani", "Bing"]
    
    private func randomName() -> String
    {
        var randomNum = Int.random(in: 0..<firstNames.count)
        let firstName = firstNames[randomNum]
        
        randomNum = Int.random(in: 0..<lastNames.count)
        let lastName = lastNames[randomNum]
        
        return firstName + " " + lastName
    }
    
    private func randomPerson() -> Person {
        let randomStatus = Person.Status.allCases.randomElement()!
        
        return Person(name: randomName(), status: randomStatus, email: "andrii.lisitsyn@gmail.com")
    }
    
    func addPeople(peopleNum: Int)
    {
        for _ in 0...peopleNum - 1
        {
            people.append(randomPerson())
        }
    }
    
    func removePeople(peopleNum: Int)
    {
        if (peopleNum < people.count)
        {
            for _ in 0...peopleNum - 1
            {
                let randomIndex = Int.random(in: 0..<people.count)
                people.remove(at: randomIndex)
            }
        }
        else
        {
            people.removeAll()
        }
    }
    
    func changeNames()
    {
        let randomNum = Int.random(in: 0..<people.count)
        
        for _ in 0...randomNum
        {
            let randomIndex = Int.random(in: 0..<people.count)
            people[randomIndex].name = randomName()
        }
    }
    
    func changeStatuses()
    {
        let randomNum = Int.random(in: 0..<people.count)
        
        for _ in 0...randomNum
        {
            let randomIndex = Int.random(in: 0..<people.count)
            people[randomIndex].status = Person.Status.allCases.randomElement()!
        }
    }
    
    func getPeople() -> [Person]
    {
        return people
    }
    
    func getCount() -> Int
    {
        return people.count
    }
}
