//
//  Person.swift
//  Contacts
//
//  Created by Andrii Lisitsyn on 5/25/20.
//  Copyright © 2020 Andrii Lisitsyn. All rights reserved.
//

import Foundation

class Person {
    var name: String
    var status: Status
    var email: String
    
    init(name: String, status: Person.Status, email: String)
    {
        self.name = name
        self.status = status
        self.email = email
    }
    
    enum Status: String, CaseIterable {
        case online = "online"
        case offline = "offline"
    }
}

