//
//  ContactsTests.swift
//  ContactsTests
//
//  Created by Andrii Lisitsyn on 5/28/20.
//  Copyright © 2020 Andrii Lisitsyn. All rights reserved.
//

import XCTest

class ContactsTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testAddPeople() {
        let peopleNum = 20
        
        People.instance.addPeople(peopleNum: peopleNum)
        
        XCTAssert(People.instance.getCount() == peopleNum)
    }
    
    func testRemovePeople() {
        let peopleNumToAdd = 20
        let peopleNumToRemove = 10
        
        People.instance.addPeople(peopleNum: peopleNumToAdd)
        People.instance.removePeople(peopleNum: peopleNumToRemove)
        
        XCTAssert(People.instance.getCount() == peopleNumToAdd - peopleNumToRemove)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}
